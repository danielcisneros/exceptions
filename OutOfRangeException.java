/**
 * A file to hold our created exception.
 * 
 * @author Daniel Cisneros 
 * @version 1.0
 * 
 */

public class OutOfRangeException extends Exception {

    /**
     *  Sets up the exception object with a particular message.
     */
    OutOfRangeException (String message) {
        super(message);
    }

}