/**
 * A arithmetic exception demo
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

public class ArithmeticException_Demo {
    public static void main(String[] args) {
        try {
            int a = 30, b = 0;
            int c = a/b;
            System.out.println("Results " + c);
        }
        catch (ArithmeticException variableName){
            System.out.println(
                "Can not divide by zero, you have beaten the robot apocolypse with this\n" +
                "Unless they are using a try-catch block\n" +
                "Then they are probably handling your exception"
            );
        }
    }   
}