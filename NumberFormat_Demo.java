/**
 * a number format demo
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

public class NumberFormat_Demo {
    public static void main(String[] args) {
        try {
            int number = Integer.parseInt("nope");
            System.out.println(number);
        } catch (NumberFormatException e) {
            System.out.println("Incorrect number format " + e.getMessage());
        }
    }
}