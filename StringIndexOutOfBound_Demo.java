/**
 * a string index out of bounds demo
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

 public class StringIndexOutOfBound_Demo {
     public static void main(String[] args) {
         try {
             String myStr = "Daniel Cisneros";
             char myChar = myStr.charAt(60);
             System.out.println(myChar);
         } catch (StringIndexOutOfBoundsException e) {
             System.out.println("String Index out of bounds\n" + e.getMessage());
             e.printStackTrace();
         }
     }
 }