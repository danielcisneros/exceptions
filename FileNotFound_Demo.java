
/**
 * a file not found demo
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class FileNotFound_Demo {
    public static void main(String[] args) {
        try {
            File myFile = new File("E://ThePerfectMan.txt");

            FileReader fr = new FileReader(myFile);
        } catch (FileNotFoundException e) {
            System.out.println("The perfect man: Ready to commit, willing to change\n" + e.getMessage());
        }
    }
}