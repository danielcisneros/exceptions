/**
 * a null pointer demo
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

public class NullPointer_Demo {
    public static void main(String[] args) {
        try {
            String a = null;
            System.out.println(a.charAt(0));
        }
        catch (NullPointerException e) {
            System.out.println("You done did it now imma pointing at nothing");
        }
    }
}