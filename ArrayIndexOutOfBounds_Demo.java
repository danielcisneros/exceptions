/**
 * a array index out of bounds demo
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

 public class ArrayIndexOutOfBounds_Demo {
     public static void main(String[] args) {
         try {
             int a[] = new int[5];

             a[42] = 9;
         } catch (ArrayIndexOutOfBoundsException e) {
             System.out.println(e.toString() + "\n" + e.getLocalizedMessage());
         }
     }
 }