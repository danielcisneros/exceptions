
/**
 * A simple project that demos user created exceptions.
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

import java.util.Scanner;

public class CreatingExceptions {
    public static void main(String[] args) throws OutOfRangeException {
        //Setup a scanner object to read from terminal.
        Scanner scan = new Scanner(System.in);
        System.out.println("Give me a number.");
        int myNumber = scan.nextInt();

        //Creating the Exception.
        final int MIN = 25, MAX = 40;
        OutOfRangeException nachoMama = new OutOfRangeException("YoLo out of range!");

        if (myNumber < MIN || myNumber > MAX) {
            throw nachoMama;
        }

        System.out.println("My number is  " + myNumber);
        scan.close();
    }
}